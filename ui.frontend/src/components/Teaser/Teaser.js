import { MapTo } from '@adobe/cq-react-editable-components';
import DOMPurify from 'dompurify';
import React, { Component } from 'react';
import './Teaser.scss';

const TeaserEditConfig = {
    emptyLabel: 'Teaser',

    isEmpty: function (props) {
         return !props || !props.title || props.title.trim().length < 1;
    }
};

class Teaser extends Component {
    render() {
        return (

            <div className="m30">
                <img className="w100" src={this.props.imagePath} alt={this.props.alt} />
                <div className="w100 cmp-teaser__content w-80 ml-8" style={{ paddingTop: '2px' }}>
                    <h2 className="cmp-teaser__title">
                        <a className="cmp-teaser__title-link" href="">{this.props.title}</a>
                    </h2>
                    <div
                        className="cmp-teaser__description"
                        data-rte-editelement
                        dangerouslySetInnerHTML={{
                            __html: DOMPurify.sanitize(this.props.description)
                        }}
                    />

                    <div className="cmp-teaser__action-container">
                        <a className="cmp-teaser__action-link" href={this.props.actions ? this.props.actions.length > 0 ? this.props.actions[0] ? this.props.actions[0].url : '' : '' : ''}>{this.props.actions ? this.props.actions.length > 0 ? this.props.actions[0] ? this.props.actions[0].title : '' : '' : ''}</a>
                    </div>
                </div>
            </div>
        )
    }
}

export default MapTo('wknd-spa-react/components/teaser')(
    Teaser,
    TeaserEditConfig
);
