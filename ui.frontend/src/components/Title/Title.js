import { MapTo } from "@adobe/cq-react-editable-components";

import React, { Component } from "react";

require("./Title.scss");

const TitleEditConfig = {
  emptyLabel: "Title",

  isEmpty: function (props) {
    return !props || !props.text || props.text.trim().length < 1;
  },
};

class Title extends Component {
  render() {
    let linkURL = React.createElement(
      "a",
      { className: "cmp-title__link", href: this.props.linkURL },
      this.props.text
    );
    let titleElement = React.createElement(
      this.props.type,
      { className: "cmp-title__text" },
      this.props.linkURL ? linkURL : this.props.text
    );
    return (
      <div className="m30">
        {!this.props.text && <p>Title</p>}
        {titleElement}
      </div>
    );
  }
}

export default MapTo("wknd-spa-react/components/title")(Title, TitleEditConfig);
