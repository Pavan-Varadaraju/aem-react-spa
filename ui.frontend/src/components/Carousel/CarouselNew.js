import { MapTo } from "@adobe/cq-react-editable-components";
import React, { Component } from "react";

import "./Carousel.scss";
import Teaser from "../Teaser/Teaser";
import Image from "../Image/Image";

const CarouselEditConfig = {
  emptyLabel: "Carousel",

  isEmpty: function (props) {
    return !props || !props.cqItems || props.cqItems.length < 1;
  },
};

class CarouselNew extends Component {
  constructor(props) {
    super(props);
    this.state = {
      slideIndex: 1,
    };
  }
  componentDidMount() {
    this.showSlides(this.state.slideIndex);
  }
  showSlides = (n) => {

    var i;
    var slides = document.getElementsByClassName("cmp-carousel__item");

    if (n > slides.length) {

      this.setState({ slideIndex: 1 }, () => {

        for (i = 0; i < 3; i++) {
          slides[i].style.display = "none";
        }
        slides[this.state.slideIndex - 1].style.display = "block";
      });
    }
    if (n < 1) {
      this.setState({ slideIndex: slides.length }, () => {
        for (i = 0; i < 3; i++) {
          slides[i].style.display = "none";
        }
        slides[this.state.slideIndex - 1].style.display = "block";
      });
    }
    if (n !== 4 && n !== 0) {
      for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
      }
      slides[this.state.slideIndex - 1].style.display = "block";
    }
  };

  plusSlides = (e) => {
    this.setState({ slideIndex: e + this.state.slideIndex }, () => {
      this.showSlides(this.state.slideIndex);
    });
  };
  currentSlide = (e) => {
    this.setState({ slideIndex: e }, () => {
      this.showSlides(this.state.slideIndex);
    });
  };

  render() {
    return (
      <>
        <div class="cmp-carousel__content">
          {this.props.cqItemsOrder &&
            this.props.cqItemsOrder.map((listItem, index) => {
              return (
                <CarouselItem
                  item={this.props.cqItemsOrder[index]}
                  items={this.props.cqItems}
                />
              );
            })}
          <a
            className="prev"
            onClick={() => {
              this.plusSlides(-1);
            }}
          >
            &#10094;
          </a>
          <a
            className="next"
            onClick={() => {
              this.plusSlides(1);
            }}
          >
            &#10095;
          </a>

          <br />
          <div style={{ textAlign: "center" }}>
            <span
              class="dot"
              onClick={() => {
                this.currentSlide(1);
              }}
            ></span>
            <span
              class="dot"
              onClick={() => {
                this.currentSlide(2);
              }}
            ></span>
            <span
              class="dot"
              onClick={() => {
                this.currentSlide(3);
              }}
            ></span>
          </div>
        </div>
      </>
    );
  }
}

class CarouselItem extends Component {
  render() {
    let carousel_Item = this.props.items[this.props.item];
    let type = ":type";

    if (carousel_Item[type] === "wknd-spa-react/components/image") {
      return (
        <div class="cmp-carousel__item fade">
          <Image
            src={carousel_Item.src}
            alt={carousel_Item.alt}
            title={carousel_Item.title}
          />
        </div>
      );
    }

    return (
      <div class="cmp-carousel__item fade">
        <Teaser
          imagePath={carousel_Item.imagePath}
          alt={carousel_Item.alt}
          title={carousel_Item.title}
          description={carousel_Item.description}
          actions={carousel_Item.actions}
        />
      </div>
    );
  }
}

export default MapTo("wknd-spa-react/components/carousel")(
  CarouselNew,
  CarouselEditConfig
);
