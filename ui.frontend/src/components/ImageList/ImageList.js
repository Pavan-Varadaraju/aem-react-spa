import { MapTo } from '@adobe/cq-react-editable-components';
import React, { Component } from 'react';

require('./ImageList.scss');

const ImageListEditConfig = {
    emptyLabel: 'ImageList',

    isEmpty: function (props) {
        // return !props || !props.text || props.text.trim().length < 1;
    }
};

class ImageList extends Component {

    getJsonData(path) {
        fetch(path + '.model.json')
            .then(res => res.json())
            .then((data) => {

            })
    }

    // render() {
    render() {

        let itemList = this.props.items.map(item =>
            <div className="cmp-image-list__item border-dark-right">
                <div className="cmp-image-list__item-title">
                    {item.title}
                    {this.getJsonData(item.path)}
                </div>
                <div className="cmp-image-list__item-description">
                    {item.description}
                </div>
            </div>
        )
        return (
            <div className="cmp-image-list">
                {itemList}
            </div>
        );
    }
}

export default MapTo('wknd-spa-react/components/image-list')(
    ImageList,
    ImageListEditConfig
);



