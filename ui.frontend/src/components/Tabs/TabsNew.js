import React, { Component } from "react";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import { MapTo } from "@adobe/cq-react-editable-components";
import "react-tabs/style/react-tabs.css";
import Text from "../Text/Text";

require("./Tabs.scss");

const TabsEditConfig = {
  emptyLabel: "Tabs",

  isEmpty: function (props) {
    return !props || !props.items || props.items.length < 1;
  },
};

class TabItem extends Component {
  render() {
    let items = this.props.items;
    console.log("tab items", items);

    return (
      <Tabs>
        <TabList>
          {items.cqItemsOrder &&
            items.cqItemsOrder.map((listItem, index) => {
              return <Tab>{items.cqItems[listItem]["cq:panelTitle"]}</Tab>;
            })}
        </TabList>

        {items.cqItemsOrder &&
          items.cqItemsOrder.map((listItem, index) => {
            let text = items.cqItems[listItem][":items"].text;
            return (
              <TabPanel>
                <Text text={text.text} richText={text.richText} />
              </TabPanel>
            );
          })}
      </Tabs>
    );
  }
}

class TabsNew extends Component {
  render() {
    return (
      <div className="cmp-tabs">
        <ol className="cmp-tabs__tablist">
          <TabItem items={this.props} />
        </ol>
      </div>
    );
  }
}

export default MapTo("wknd-spa-react/components/tabs")(TabsNew, TabsEditConfig);
