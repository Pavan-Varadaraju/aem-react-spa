import { MapTo } from '@adobe/cq-react-editable-components';
import React, { Component } from 'react';

const TextEditConfig = {
  emptyLabel: 'Form Text',

  isEmpty: function (props) {
    return !props || !props.type || props.type.trim().length < 1;
  }
};

class Text extends Component {
  render() {
    return (
      <>
        {this.props.title.length > 0 &&
          <div className="m10"><label className="title">{this.props.title}</label></div>
        }
        {
          this.props.type === "textarea" &&
          <textarea className="m10 w250" id={this.props.id} placeholder={this.props.placeholder} name={this.props.name} rows={this.props.rows}></textarea>
        }
        {
          (this.props.type === "text" || this.props.type === "email" || this.props.type === "tel" || this.props.type === "date" || this.props.type === "number" || this.props.type === "password") &&
          <input className="mb20 w250" id={this.props.id} name={this.props.name} type={this.props.type} placeholder={this.props.placeholder}></input>
        }
        {
          (this.props.helpMessage && !this.props.placeholder) &&
          <span className="helpText ml5">{this.props.helpMessage}</span>
        }
      </>
    )
  }
}

export default MapTo('wknd-spa-react/components/form/text')(
  Text,
  TextEditConfig
);